# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class WuyoujobItem(scrapy.Item):
    # 公司
    company = scrapy.Field()
    # 职位
    job = scrapy.Field()
    # 薪水
    salary = scrapy.Field()
    # 地址
    location = scrapy.Field()
    time = scrapy.Field()
