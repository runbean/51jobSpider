# -*- coding: utf-8 -*-
import scrapy
import re
from wuyoujob.items import WuyoujobItem


class WuyouSpider(scrapy.Spider):
    name = 'wuyou'
    # allowed_domains = ['search.51job.com']
    start_urls = ['https://search.51job.com/list/200200,000000,0000,00,9,99,java,2,1.html']


    def parse(self, response):
        # 方式一:
        # for each in response.xpath("//div[@class='dw_table']//div[@class='el']"):
        #     item = WuyoujobItem()
        #     try:
        #         item['job'] = each.xpath("./p/span/a/@title").extract()[0].encode("utf-8")
        #         item['company'] = each.xpath("./span/a/@title").extract()[0].encode("utf-8")
        #         item['location'] = each.xpath("./span[@class='t3']/text()")[0].extract().encode("utf-8")
        #         item['salary'] = each.xpath("./span[@class='t4']/text()")[0].extract().encode("utf-8")
        #         item['time'] = each.xpath("./span[@class='t5']/text()")[0].extract().encode("utf-8")
        #         curpage = re.search('(\d+\\.html)', response.url).group(1)
        #         page = int(re.search('(\d+)', curpage).group(1)) + 1
        #         url = re.sub('\d+\\.html', str(page)+".html", response.url)
        #         yield scrapy.Request(url, callback=self.parse)
        #         yield item
        #     except Exception:
        #         pass
        # 方式二:
        # 下页招聘信息链接
        next_url = response.xpath("//div[@class='p_in']/ul/li[@class = 'bk'][last()]/a/@href").extract_first()
        for content_url in response.xpath("//div[@class='el']/p/span/a/@href"):
            yield scrapy.Request(content_url.extract(), callback=self.content_parse)
        if next_url is not None:
            yield scrapy.Request(next_url, callback=self.parse)

    @classmethod
    def content_parse(self, response):
            item = WuyoujobItem()
            try:
                result = ""
                contents = response.xpath("//div[@class='tBorderTop_box'][1]//div[@class='bmsg job_msg inbox']/p")
                for content in reversed(contents.extract()):
                    result = content + result + "\n"
                item['job'] = result.strip().replace("<p>", "").replace("</p>", "")
                yield item
            except Exception :
                pass
